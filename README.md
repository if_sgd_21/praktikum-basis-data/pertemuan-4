Materi Pertemuan 4

Data Manipulation Language
Perintah untuk memanipulasi isi data dari Table pada Database, yang melingkupi perintah:

Penambahan data (INSERT)
Pengubahan data (UPDATE)
Penghapusan data (DELETE)
PENAMBAHAN DATA (INSERT)
Berfungsi untuk menambahkan data ke Table

-- Contoh: Menambah lima data ayam ke table
INSERT INTO ayam(id, panggilan, umur, jenis_kelamin)
VALUES (1, 'Front Strike', 2.4, 'JANTAN'),
(2, 'Germany Ambush', 3.0, 'JANTAN'),
(3, 'White Royco', 2.9, 'BETINA'),
(4, 'Silent Recovery', 5.7, 'BETINA'),
(5, 'Little Moscow', 0.8, 'JANTAN')
PENGUBAHAN DATA (UPDATE)
Berfungsi untuk mengubah data. Keyword WHERE digunakan untuk menentukan baris / item data mana yang akan diubah. Hati-hati jika tidak menggunakan WHERE maka seluruh data akan terubah.

-- Contoh: Mengubah ayam yang panggilannya 'Sky For Sadness' menjadi 'Sky For Happiness' dan umurnya menjadi 3.0
UPDATE ayam
SET panggilan = 'Sky For Happiness',
umur = 3.0
WHERE panggilan = 'Sky For Sadness'
PENGHAPUSAN DATA (DELETE)
Berfungsi untuk menghapus data. Keyword WHERE digunakan untuk menentukan baris / item data mana yang akan dihapus. Hati-hati jika tidak menggunakan WHERE maka seluruh data akan terhapus.

-- Contoh: Menghapus data ayam yang pekerjaannya MATA-MATA dan tingkat_bahayanya diatas 75.3
DELETE FROM ayam
WHERE pekerjaan = 'MATA-MATA'
AND tingkat_bahaya > 75.3


Praktikum
Tools: https://sqliteonline.com



DATA DEFINITION LANGUAGE
CREATE TABLE hero (
	hero_id INT AUTO_INCREMENT PRIMARY KEY,
  hero_name VARCHAR(40),
  price INT
);
DATA MANIPULATION LANGUAGE
INSERT
INSERT INTO hero(hero_name, price)
VALUES 
	('Miya', 19000),
	('Balmond', 28700),
   ('Nana', 15000)
;
UPDATE
UPDATE hero
SET price = 34600
WHERE hero_id = 1;
DELETE
DELETE FROM hero
WHERE price < 20000;
MYSQL CHEAT SHEET
https://devhints.io/mysql

https://buggyprogrammer.com/mysql-cheatsheet-pdf/

https://www.tutorialspoint.com/mysql/mysql-insert-query.htm

https://www.interviewbit.com/mysql-cheat-sheet/#dml-commands

MYSQL BUILT IN FUNCTIONS
https://www.tutorialspoint.com/mysql/mysql-date-time-functions.htm

https://www.tutorialspoint.com/mysql/mysql-numeric-functions.htm

https://www.tutorialspoint.com/mysql/mysql-string-functions.htm

IT SKILLS ROADMAP
https://roadmap.sh/roadmaps

MISC.
Coba-coba hosting murah-meriah bikin aplikasi sendiri 

https://idcloudhost.com/hosting/

https://www.qwords.com/hosting-murah/
