CREATE TABLE `Users` (
  `id` integer PRIMARY KEY,
  `username` varchar(255),
  `first_name` varchar(255),
  `last_name` varchar(255),
  `email` varchar(255),
  `password` varchar(255),
  `profil_pic` varchar(255),
  `bio` varchar(255),
  `location` varchar(255),
  `birth_date` DATE,
  `followers` integer,
  `following` integer,
  `join_date` DATE
);

CREATE TABLE `Followers` (
  `id_followers` integer PRIMARY KEY,
  `id_user` int
);

CREATE TABLE `Following` (
  `id_following` integer PRIMARY KEY,
  `id_user` int
);

CREATE TABLE `Tweets` (
  `id_tweet` integer PRIMARY KEY,
  `id_user` int,
  `tweet` TEXT,
  `tweet_date` DATETIME,
  `retweet_count` integer,
  `like_count` integer
);

CREATE TABLE `Mentions` (
  `id_tweet` integer,
  `target` integer,
  `source` integer
);

CREATE TABLE `Replies` (
  `id_reply` integer PRIMARY KEY,
  `id_user` integer,
  `id_tweet` integer,
  `reply` TEXT,
  `reply_date` DATETIME,
  `reply_to_tweet_id` integer
);

CREATE TABLE `DM` (
  `message` TEXT,
  `sender` integer,
  `receiver` integer,
  `dm_date` DATETIME
);

ALTER TABLE `Followers` ADD FOREIGN KEY (`id_user`) REFERENCES `Users` (`id`);

ALTER TABLE `Following` ADD FOREIGN KEY (`id_user`) REFERENCES `Users` (`id`);

ALTER TABLE `Tweets` ADD FOREIGN KEY (`id_user`) REFERENCES `Users` (`id`);

ALTER TABLE `Mentions` ADD FOREIGN KEY (`id_tweet`) REFERENCES `Tweets` (`id_tweet`);

ALTER TABLE `Mentions` ADD FOREIGN KEY (`target`) REFERENCES `Users` (`id`);

ALTER TABLE `Mentions` ADD FOREIGN KEY (`source`) REFERENCES `Tweets` (`id_user`);

ALTER TABLE `Replies` ADD FOREIGN KEY (`id_user`) REFERENCES `Users` (`id`);

ALTER TABLE `Replies` ADD FOREIGN KEY (`id_tweet`) REFERENCES `Tweets` (`id_tweet`);

ALTER TABLE `Replies` ADD FOREIGN KEY (`reply_to_tweet_id`) REFERENCES `Tweets` (`id_tweet`);

ALTER TABLE `DM` ADD FOREIGN KEY (`sender`) REFERENCES `Users` (`id`);

ALTER TABLE `DM` ADD FOREIGN KEY (`receiver`) REFERENCES `Users` (`id`);
