INSERT INTO Users (id, username, first_name, last_name, email, password, bio, location, birth_date, followers, following, join_date)
VALUES 
  (1, 'johndoe', 'John', 'Doe', 'johndoe121@gmail.com', '123456', NULL, 'Ohio', NULL, 123, 312, '2020-01-01'),
  (2, 'janedoe', 'Jane', 'Doe', 'janedoe321@gmail.com', 'abcdef', 'I love hiking and photography', 'California', '1995-06-15', 456, 789, '2021-03-15'),
  (3, 'sarahlee', 'Sarah', 'Lee', 'sarahlee@gmail.com', 'password123', 'Avid reader and traveler', 'New York', '1990-02-28', 256, 398, '2018-07-07'),
  (4, 'mikejohnson', 'Mike', 'Johnson', 'mikejohnson@yahoo.com', 'qwerty', 'Tech enthusiast and gamer', 'Texas', '1987-12-10', 812, 431, '2015-02-19'),
  (5, 'amanda_smith', 'Amanda', 'Smith', 'amanda.smith@hotmail.com', 'P@ssw0rd', 'Music lover and foodie', 'Florida', '1998-04-02', 321, 654, '2019-11-30'),
  (6, 'robertjones', 'Robert', 'Jones', 'robert.jones@gmail.com', 'abc123', 'Sports fanatic and fitness enthusiast', 'California', '1993-09-22', 764, 128, '2017-05-10'),
  (7, 'emilybrown', 'Emily', 'Brown', 'emily.brown@yahoo.com', 'letmein', 'Aspiring writer and artist', 'New York', '1996-11-12', 219, 584, '2016-01-25'),
  (8, 'peterwang', 'Peter', 'Wang', 'peter.wang@gmail.com', 'password456', 'Coding is my passion', 'California', '1991-07-17', 659, 312, '2014-09-05'),
  (9, 'samanthasmith', 'Samantha', 'Smith', 'samantha.smith@hotmail.com', 'qazwsx', 'Movie buff and animal lover', 'Texas', '1997-05-06', 467, 238, '2022-02-14'),
  (10, 'davidnguyen', 'David', 'Nguyen', 'david.nguyen@gmail.com', 'welcome123', 'Coffee addict and aspiring chef', 'Texas', '1994-01-30', 523, 197, '2021-08-08');
